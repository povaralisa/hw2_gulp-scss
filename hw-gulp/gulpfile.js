
// *** Modules *** //
const gulp = require('gulp'),
    concat =  require('gulp-concat'),
    clean = require('gulp-clean'),
    browserSync = require('browser-sync').create(),
     sass = require('gulp-sass');



    const paths = {
        src: {
            scss: './src/scss/**/*.scss',
            js: './src/js/*.js',
        },
        build: {
            css: './build/css/',
            js: './build/js/',
            self: './build',
        },
    }

    //*** Function **** //
    const cssBuild = () => 
    gulp.src(paths.src.scss)
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest(paths.build.css))
    .pipe(browserSync.stream());

    const jsBuild = () =>
    gulp.src(paths.src.js)
    .pipe(concat('script.js'))
    .pipe(gulp.dest(paths.build.js))
    .pipe(browserSync.stream());
    
    const cleanBuild = () =>
     gulp.src(paths.build.self,{allowEmpty:true})
     .pipe(clean())

     const watcher = () => {
        browserSync.init({
            server: {
                baseDir: './',
            },
        })
        gulp.watch(paths.src.scss, cssBuild).on('change', browserSync.reload)
        gulp.watch(paths.src.js, jsBuild).on('change', browserSync.reload)

    }
// gulp.task('htmlBuild',() => 
//     gulp.src('./src/index.html')
//     .pipe(gulp.dest('./build/')
//     ))

    ////**** Tasks **** //
    gulp.task('cssBuild',cssBuild)
    gulp.task('jsBuild',jsBuild)
    gulp.task('cleanBuild',cleanBuild)
    gulp.task('watcher', watcher)
        

        gulp.task('default',gulp.series(cleanBuild,cssBuild,jsBuild,watcher))